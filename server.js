// server.js

// set up ======================================================================
// get all the tools we need
var path = require('path');
var express  = require('express');
var app      = express();
var port     = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');

var redis = require("redis").createClient();
var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var bodyParser = require('body-parser');
var configDB = require('./config/database.js');
app.engine('html', require('ejs').renderFile);
// configuration ===============================================================
mongoose.connect(configDB.url); // connect to our database

require('./config/passport')(passport); // pass passport for configuration

app.configure(function() {

	// set up our express application
	app.use(express.logger('dev')); // log every request to the console
	app.use(express.cookieParser()); // read cookies (needed for auth)
   
    app.use(express.session({
    secret:"qewqw132", // Keep your secret key 
    key:"connect.sid", 
    cookie: { maxAge:86400000 },
    store:new RedisStore({
        host: 'localhost',
        port: 6379,
       client:redis
        
    })
}));
   
	app.use(express.bodyParser()); // get information from html forms
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'ejs');
	
	// required for passport
	/*app.use(ConnectRedisSessions({'app':'test', cookie: {ttl: 72000}, debug:true}));*/
	app.use(passport.initialize());
	app.use(passport.session()); // persistent login sessions
	app.use(flash()); // use connect-flash for flash messages stored in session
	

});
 console.log(session.store);
app.all('*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// launch ======================================================================

module.exports=app;
app.listen(port);
console.log('The magic happens on port ' + port);
