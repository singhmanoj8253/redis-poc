// app/routes.js
module.exports = function(app, passport) {

	// =====================================
	// HOME PAGE (with login links) ========
	// =====================================
	app.get('/', function(req, res) {
        console.log("in root:"+req.sessionID);
		if(req.session.passport.user!==null && req.isAuthenticated()){
		
		res.redirect('/profile'); 
		
		}
		else
        	console.log('after else'+req.sessionID);
		res.render('index.ejs');
	});

	// =====================================
	// LOGIN ===============================
	// =====================================
	// show the login form
	app.get('/login', function(req, res) {

		// render the page and pass in any flash data if it exists
		res.render('login.ejs', { message: req.flash('loginMessage') });
	});

	// process the login form
app.post('/login', function (req, res){
    req.session.save();
    console.log("in login:"+req.sessionID);
    passport.authenticate('local-login', function(err, user, info){
        
        console.log(user);
        if (err) return res.redirect("/");
        if (!user) return res.redirect('/login');

        else {
           
            req.login(user, function(err) {
              if (err) return next(err);
              console.log("Request Login successful.");
              return res.redirect('/profile');
                console.log("kjadgd");
            });
            
        }
    })(req, res);
});
	// =====================================
	// SIGNUP ==============================
	// =====================================
	// show the signup form
	app.get('/signup', function(req, res) {

		// render the page and pass in any flash data if it exists
		res.render('signup.ejs', { message: req.flash('signupMessage') });
	});

	// process the signup form
	app.post('/signup',function(req, res, next) {
  passport.authenticate('local-signup', function(err, user, info) {
    if (err) { return next(err); }
    if (!user) { return res.redirect('/login'); }
    req.logIn(user, function(err) {
      if (err) { return next(err); }
      return res.redirect('/profile');
    });
  })(req, res, next);
	});

	// =====================================
	// PROFILE SECTION =========================
	// =====================================
	// we will want this protected so you have to be logged in to visit
	// we will use route middleware to verify this (the isLoggedIn function)
	app.get('/profile', isLoggedIn, function(req, res) {
		res.render('profile.ejs', {
			user : req.user // get the user out of session and pass to template
            
		});
        //console.log(res);
         
	});

	// =====================================
	// LOGOUT ==============================
	// =====================================
	app.get('/logout', function(req, res) {
		req.logout();
		
		res.redirect('/');
		req.session.destroy();

	});
};

// route middleware to make sure
function isLoggedIn(req, res, next) {
     console.log(req.isAuthenticated());
	// if user is authenticated in the session, carry on
	if (req.isAuthenticated())
    {
		return next();
    }
    
	// if they aren't redirect them to the home page
	res.redirect('/login');
    
}
